import React from 'react'
import Share from './Share'
import {getCode, setCode} from './cache'
import CardContent from './CardContent'
import CampaignDate from './CampaignDate'
import './gift.css'
import campaign from './campaign.json'

class Campaign extends React.Component {
	constructor(props) {
		super(props)
		const path = '711eec4d-c158-449c-a426-00fefc6e5416'
		this.state = {
			code : getCode(path),
			path : path,
			campaign : campaign,
		}
		this.generateCode = this.generateCode.bind(this)
	}

	generateCode() {
		setCode(this.state.path)
		this.setState(prevState => ({
			code : getCode(this.state.path)
		}));
	}

	render() {
		const campaign = this.state.campaign
		console.log(new Date(campaign.date.end))
		return (
		<div className="content">
			<div className='card'>
				<h1 className='card-header'>
					{campaign.name}
				</h1>
				<div className='card-body'>
					<div className='terms'>
						{campaign.description}
					</div>
					<div className='terms'>
						Акция будет проходить в период с <CampaignDate date={new Date(campaign.date.start)} /> по <CampaignDate date={new Date(campaign.date.end)} />
					</div>
					<div className='terms'>
						Организатор: <a
								rel="noopener noreferrer"
								target="_blank"
								href={campaign.url}
							>
							{campaign.name}
							</a>
					</div>
					<CardContent
						code={this.state.code}
						onGetCode={this.generateCode}
						endDate={new Date(campaign.date.end)}
					/>
				</div>
			</div>
			<Share />
		</div>
	)}
}

export default Campaign