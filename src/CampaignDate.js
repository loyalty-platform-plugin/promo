import React from 'react'

const CampaignDate = ({date}) => (
	<span>
		{date.getDate()}.{date.getMonth() + 1}.{date.getFullYear()}
	</span>
)

export default CampaignDate