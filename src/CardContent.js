import React from 'react'
import Expired from './Expired'
import GetCode from './GetCode'
import Code from './Code'

const CardContent = ({code, onGetCode, endDate}) => {
	let now = new Date();
	now.setDate(now.getDate() - 1);
	console.log(now, endDate)
	if (now > endDate)
		return <Expired />
	return code ? <Code code={code} /> : <GetCode onGetCode={onGetCode} />
}

export default CardContent