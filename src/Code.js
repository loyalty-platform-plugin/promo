import React from 'react'

const Code = ({code}) => (
	<div className='code'>
		Ваш промо-код: 
		<span className='button value'>
			{code}
		</span>
	</div>
)

export default Code