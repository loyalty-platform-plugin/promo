import React from 'react'

const GetCode = ({onGetCode}) => (
	<div
		className='button card-action'
		onClick={onGetCode}
	>
		Получить подарок
	</div>
)

export default GetCode