import React from 'react'

const content = [{
	title : "Facebook",
	href : "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Floyalty-platform-plugin.gitlab.io%2Fpromo&t=Loyalty%20Platform",
	img : "img/Facebook.png",
},{
	href : "https://twitter.com/intent/tweet?source=https%3A%2F%2Floyalty-platform-plugin.gitlab.io%2Fpromo&text=Loyalty%20Platform:%20https%3A%2F%2Floyalty-platform-plugin.gitlab.io%2Fpromo",
	title : "Tweet",
	img : "img/Twitter.png",
},{
	href : "https://plus.google.com/share?url=https%3A%2F%2Floyalty-platform-plugin.gitlab.io%2Fpromo",
	title : "Share on Google+",
	img : "img/Google+.png",
},{
	href : "mailto:?subject=Loyalty%20Platform&body=%D0%9D%D0%B0%D1%87%D0%BD%D0%B8%D1%82%D0%B5%20%D0%BF%D1%80%D0%B8%D0%B2%D0%BB%D0%B5%D0%BA%D0%B0%D1%82%D1%8C%20%D0%BD%D0%BE%D0%B2%D1%8B%D1%85%20%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%20%D1%81%D0%BE%D0%B2%D0%B5%D1%80%D1%88%D0%B5%D0%BD%D0%BD%D0%BE%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%BE:%20https%3A%2F%2Floyalty-platform-plugin.gitlab.io%2Fpromo",
	title : "Send email",
	img : "img/Email.png",
}]

const Item = ({href, title, img}) => (
	<li>
		<a
			rel="noopener noreferrer"
			href={href}
			title={title}
			target="_blank"
		>
			<img
				alt={title}
				src={img}
			/>
		</a>
	</li>
)


const Share = ()=> (
	<ul className="share-buttons">
		{content.map((item, idx) => {
			return <Item
				{...item}
				key={idx}
			/>
		})}
	</ul>		
)

export default Share