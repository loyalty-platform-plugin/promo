import Cookies from 'js-cookie'

const getRandomNumber = (min,max) => Math.floor(Math.random() * (max - min)) + min
const getCode = (path) => Cookies.get(path)
const setCode = (path) => Cookies.set(path, getRandomNumber(10000, 15000))

export {
	getCode,
	setCode,
}