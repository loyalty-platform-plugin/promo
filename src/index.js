import React from 'react'
import {render} from 'react-dom'
import Campaign from './Campaign'
import ReactGA from 'react-ga'

ReactGA.initialize('UA-110489793-1')

ReactGA.event({
  category: 'Promotion',
  action: 'Displayed Promotional Widget',
  label: 'Homepage',
  nonInteraction: true
})

ReactGA.pageview('/plugin')

render(
	<Campaign />,
	document.getElementById('loyalty_platform_promo_plugin')
);
